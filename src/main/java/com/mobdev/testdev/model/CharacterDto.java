package com.mobdev.testdev.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

@Data
@Builder
public class CharacterDto implements Serializable {
    private int id;
    private String name;
    private String status;
    private String species;
    private String type;
    private Origin origin;
    private ArrayList<String> episode = new ArrayList<>();
}
