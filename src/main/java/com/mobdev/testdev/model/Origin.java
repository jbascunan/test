package com.mobdev.testdev.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

@Data
@Builder
public class Origin implements Serializable {
    private String name;
    private String url;
    private String dimension;
    ArrayList<String> residents = new ArrayList<>();
}
