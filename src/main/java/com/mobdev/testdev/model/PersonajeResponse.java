package com.mobdev.testdev.model;

import lombok.Builder;
import lombok.Data;
import java.io.Serializable;

@Data
@Builder
public class PersonajeResponse implements Serializable {
    private int id;
    private String name;
    private String status;
    private String species;
    private String type;
    private int episode_count;
    private Origin origin;
}
