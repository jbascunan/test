package com.mobdev.testdev.controller;

import com.mobdev.testdev.model.PersonajeResponse;
import com.mobdev.testdev.service.PersonajeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(path = "/personaje")
public class PersonajeController {

    private final PersonajeService personajeService;

    public PersonajeController(PersonajeService personajeService) {
        this.personajeService = personajeService;
    }

    @GetMapping("/{id}")
    @ResponseBody
    public PersonajeResponse getPersonaje(@PathVariable("id") int id) throws Exception {
        return this.personajeService.getPersonaje(id);
    }
}
