package com.mobdev.testdev.gateway;

import com.mobdev.testdev.model.CharacterDto;
import com.mobdev.testdev.model.Origin;

public interface ApiGateway {
    CharacterDto getCharacter(int id) throws Exception;
    Origin getOrigin(String url);
}
