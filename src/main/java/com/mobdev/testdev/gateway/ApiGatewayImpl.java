package com.mobdev.testdev.gateway;

import com.mobdev.testdev.model.CharacterDto;
import com.mobdev.testdev.model.Origin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.HttpMethod.GET;

@Component
public class ApiGatewayImpl implements ApiGateway{
    private final RestTemplate restTemplate;

    @Value("${rickandmortyapi.host}")
    private String apiHost;

    @Value("${rickandmortyapi.endpoint-character}")
    private String endpointCharacter;

    public ApiGatewayImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public CharacterDto getCharacter(int id) throws Exception {
        String url = apiHost.concat(endpointCharacter);
        ResponseEntity<CharacterDto> response = callExternalApi(url, id, CharacterDto.class);

        if(response != null && response.getBody() != null)
            return response.getBody();
        else
            throw new Exception("Sin resultados por id ingresado");
    }

    @Override
    public Origin getOrigin(String url) {
        ResponseEntity<Origin> response = callExternalApi(url, 0, Origin.class);

        return response.getBody();
    }

    private <T> ResponseEntity<T> callExternalApi(String endpoint, int id, Class<T> clazz) {
        HttpEntity<T> request = new HttpEntity<>(new HttpHeaders());
        return restTemplate.exchange(endpoint, GET, request, clazz, id);
    }
}
