package com.mobdev.testdev.service;

import com.mobdev.testdev.gateway.ApiGateway;
import com.mobdev.testdev.model.CharacterDto;
import org.apache.commons.lang3.StringUtils;
import com.mobdev.testdev.model.PersonajeResponse;
import org.springframework.stereotype.Service;

@Service
public class PersonajeServiceImpl implements PersonajeService {
    private final ApiGateway apiGateway;

    public PersonajeServiceImpl(ApiGateway apiGateway) {
        this.apiGateway = apiGateway;
    }

    @Override
    public PersonajeResponse getPersonaje(int id) throws Exception {
            CharacterDto characterDto = this.apiGateway.getCharacter(id);

            String url = StringUtils.EMPTY;
            int episode_count =  characterDto.getEpisode() != null ? characterDto.getEpisode().size() : 0;

            PersonajeResponse personajeResponse = PersonajeResponse.builder()
                    .id(characterDto.getId())
                    .name(characterDto.getName())
                    .species(characterDto.getSpecies())
                    .status(characterDto.getStatus())
                    .episode_count(episode_count)
                    .type(characterDto.getType())
                    .build();

            if(characterDto.getOrigin() != null)
                url = characterDto.getOrigin().getUrl();

            if (StringUtils.isNotEmpty(url)) {
                personajeResponse.setOrigin(this.apiGateway.getOrigin(characterDto.getOrigin().getUrl()));
            }

            return personajeResponse;

    }
}
