package com.mobdev.testdev.service;

import com.mobdev.testdev.model.PersonajeResponse;

public interface PersonajeService {
    PersonajeResponse getPersonaje(int id) throws Exception;
}
