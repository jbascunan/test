package com.mobdev.testdev;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TestdevApplicationTests {
	private static final String PATH_SCHEMA = "/schema.json";
	private static final String PATH_JSON_VALID = "/personaje_valid.json";
	private static final String PATH_JSON_INVALID = "/personaje_invalid.json";
	private JSONObject jsonSchema;

	@BeforeEach
	public void init() {
		jsonSchema = new JSONObject(new JSONTokener(getClass().getResourceAsStream(PATH_SCHEMA)));
	}

	@Test
	public void jsonValid() throws ValidationException {
		JSONObject jsonSubject = new JSONObject(new JSONTokener(getClass().getResourceAsStream(PATH_JSON_VALID)));

		Schema schema = SchemaLoader.load(jsonSchema);
		schema.validate(jsonSubject);
	}

	@Test
	public void jsonInvalid() {
		JSONObject jsonSubject = new JSONObject(new JSONTokener(getClass().getResourceAsStream(PATH_JSON_INVALID)));

		Schema schema = SchemaLoader.load(jsonSchema);

		ValidationException thrown = Assertions.assertThrows(ValidationException.class, () -> {
			schema.validate(jsonSubject);
		});

		Assertions.assertEquals("required key [id] not found", thrown.getErrorMessage());
	}

}
