package com.mobdev.testdev.service;

import com.mobdev.testdev.gateway.ApiGatewayImpl;
import com.mobdev.testdev.model.CharacterDto;
import com.mobdev.testdev.model.Origin;
import com.mobdev.testdev.model.PersonajeResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

class PersonajeServiceImplTest {
    @Mock
    private ApiGatewayImpl apiGateway;
    private PersonajeServiceImpl personajeService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        personajeService = new PersonajeServiceImpl(apiGateway);
    }

    @Test
    void getPersonaje() throws Exception {
        CharacterDto characterDto = CharacterDto.builder().id(1).name("test").origin(Origin.builder().url("testUrl").build()).build();
        Origin origin = Origin.builder().dimension("testDimesion").url("http://test").build();
        Mockito.when(apiGateway.getCharacter(1)).thenReturn(characterDto);
        Mockito.when(apiGateway.getOrigin("testUrl")).thenReturn(origin);

        PersonajeResponse personajeResponse = personajeService.getPersonaje(1);
        Assertions.assertNotNull(personajeResponse);
        Assertions.assertEquals(1, personajeResponse.getId());
    }
}